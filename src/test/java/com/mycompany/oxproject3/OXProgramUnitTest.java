/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxproject3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
//     @Test
//     public void testAdd_num1_1_num2_2_output_3() {
//         int result = Calculation.add(1,2);
//         assertEquals(3, result);
//     }
    @Test
    public void testCheckwin_NoOnePlay_By_O() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_NoOnePlay_By_X() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_row0_By_X() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_row1_By_X() {
        String[][] table = {{"-", "-", "-"}, {"X", "X", "X"}, {"O", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_row2_By_O() {
        String[][] table = {{"-", "-", "-"}, {"-", "X", "X"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_col0_By_O() {
        String[][] table = {{"O", "-", "-"}, {"O", "X", "X"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_col1_By_X() {
        String[][] table = {{"-", "X", "-"}, {"O", "X", "-"}, {"O", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_col2_By_X() {
        String[][] table = {{"-", "O", "X"}, {"O", "-", "X"}, {"O", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_digonal1_By_X() {
        String[][] table = {{"X", "-", "-"}, {"O", "X", "-"}, {"O", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckwin_digonal2_By_O() {
        String[][] table = {{"-", "-", "O"}, {"X", "O", "-"}, {"O", "-", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProject3.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDraw_Full() {
        String[][] table = {{"X", "O", "X"}, {"X", "O", "O"}, {"O", "X", "X"}};
        String currentPlayer = "O";
        assertEquals(false, OXProject3.checkWin(table, currentPlayer));
        assertEquals(true, OXProject3.checkDraw(table));
    }

    @Test
    public void testCheckDraw_NotFull() {
        String[][] table = {{"-", "O", "X"}, {"X", "-", "O"}, {"O", "X", "X"}};
        String currentPlayer = "O";
        assertEquals(false, OXProject3.checkWin(table, currentPlayer));
        assertEquals(false, OXProject3.checkDraw(table));
    }

}
