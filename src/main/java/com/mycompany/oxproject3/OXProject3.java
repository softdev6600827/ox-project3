/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxproject3;

/**
 *
 * @author informatics
 */
public class OXProject3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }

        if (checkCol(table, currentPlayer)) {
            return true;
        }

        if (checkDiagonal(table, currentPlayer)) {
            return true;
        }

        return false;
    }

    private static boolean checkDiagonal(String[][] table, String currentPlayer) {
        if (checkDiagonal1(table, currentPlayer) || checkDiagonal2(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    private static boolean checkDiagonal1(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }

    private static boolean checkDiagonal2(String[][] table, String currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }

    static boolean checkDraw(String[][] table) {
        return checkTableFull(table);
    }

    private static boolean checkTableFull(String[][] table) {
        for(int row=0; row<3; row++){
            for(int col=0; col<3; col++){
                if(table[row][col].equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }
}
